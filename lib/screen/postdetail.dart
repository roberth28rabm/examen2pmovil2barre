import 'package:examenbarre/model/post.dart';
import 'package:flutter/material.dart';

class PostDetail extends StatelessWidget {
  final Post post;

  PostDetail({required this.post});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(post.title),
          backgroundColor: Colors.pink,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        title: Text("ID: " + "${post.id}",
                            style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                fontSize: 13.0)),
                      ),
                      ListTile(
                        title: Text("User ID: " + "${post.userId}",
                            style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                fontSize: 13.0)),
                      ),
                      ListTile(
                        title: Text("Title: " + post.title,
                            style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                fontSize: 13.0)),
                      ),
                      ListTile(
                        title: Text("Body: ",
                            style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                fontSize: 13.0)),
                        subtitle:
                            Text(post.body, style: TextStyle(fontSize: 12.0)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
