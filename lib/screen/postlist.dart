import 'package:examenbarre/model/post.dart';
import 'package:examenbarre/request/posthttprequest.dart';
import 'package:examenbarre/screen/postdetail.dart';
import 'package:flutter/material.dart';

class PostsPages extends StatelessWidget {
  final PostHttpRequest postHttpRequest =
      PostHttpRequest(); //Proporciona una forma fácil de obtener información de una URL sin tener que recargar la página completa.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('API REST pública  HTTP'),
        backgroundColor: Colors.pink,
      ),
      body: FutureBuilder(
        future: postHttpRequest.getPosts(), //Obtener los datos del servidor
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            List<Post> post = snapshot
                .data; //simplifica el acceso y la conversión de propiedades en un objeto similar a JSON, por ejemplo, un objeto JSON devuelto por un servicio REST-api
            return ListView(
              children: post
                  .map(
                    (Post post) => ListTile(
                      /*trailing: Text('${post.id}',
                          style: TextStyle(
                              overflow: TextOverflow.ellipsis, fontSize: 12.0)),*/
                      title: Text(post.title,
                          style: TextStyle(
                              overflow: TextOverflow.ellipsis, fontSize: 14.0)),
                      subtitle: Text(
                        "${post.body}",
                        style: TextStyle(fontSize: 12.0),
                        maxLines: 1,
                      ),
                      leading: CircleAvatar(
                          backgroundColor: Colors.pink,
                          child: Text(
                              '${post.title.substring(0, 1)}')), //Toma la primera letra del titulo y la convierte en avatar de la lista
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          //reemplaza toda la pantalla con una transición adaptable a la plataforma
                          builder: (context) => PostDetail(
                            post: post,
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
