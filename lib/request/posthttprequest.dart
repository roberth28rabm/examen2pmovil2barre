//Servicio que me permitira consumir los datos

import 'dart:convert';

import 'package:examenbarre/model/post.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

class PostHttpRequest {
  //Obtener los datos del servidor
  Future<List<Post>> getPosts() async {
    Response res =
        await get(Uri.parse("https://jsonplaceholder.typicode.com/posts"));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<Post> posts = body
          .map(
            (dynamic item) => Post.fromJson(
                item), //convierte el Map json en un Post usando el método fromJson de tipo factory
          )
          .toList();

      return posts; //presenta los datos de la peticion
    } else {
      throw "Can't get posts.";
    }
  }
}
